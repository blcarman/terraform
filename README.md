This is an in progress set of terraform modules I use for my homelab. Long term goal is to have my lab fully defined here. Currently it does some vcenter newtork config and can create VMs.

## Deploying New VMs ##

Add the following updating the variables as needed to deploy a set of VMs with a base chef config.

```
module "testvm" {
  source           = "../modules/vm"
  network_name     = var.trust_net["vsphere_name"]
  cidr             = var.trust_net["cidr"]
  gateway          = var.trust_net["gateway"]
  domain           = "lab.bradlab.tech"
  chef_user        = var.CHEF_USER
  chef_key         = var.CHEF_KEY
  chef_group       = "testgroup"
  initial_password = "Pa55word"
  vm_name_prefix   = "node"
  # optional entries
  vm_count         = 1
}
```