variable "environment" {
  description = "demo environment name derived from terraform workspace"
  type = "string"
  default = "dev"
}

variable "VC_USERNAME" {
  default = "notmyuser"
}

variable "VC_PASSWORD" {                                                                                    
  type    = string
  default = "notmypassword"
}

variable "vsphere_server" { 
  default = "vcenter.example.com"
}
