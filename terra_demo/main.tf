data "vsphere_datacenter" "dc" {                                                                            
  name = "HomeLab"                                                                                          
}

resource "vsphere_folder" "folder" {
  path          = "demo_${var.environment}"
  type          = "vm"
  datacenter_id = data.vsphere_datacenter.dc.id
}
