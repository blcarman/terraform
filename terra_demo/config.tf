terraform {                                                                     
  backend "remote" {                                                            
    hostname     = "app.terraform.io"
    organization = "bradlab"
    workspaces {
      name = "infra-dev"
    }
  }
}

provider "vsphere" {                                                                                        
  user                 = var.VC_USERNAME                                                                    
  password             = var.VC_PASSWORD                                                                    
  vsphere_server       = var.vsphere_server                                                                 
  allow_unverified_ssl = true                                                                               
}
