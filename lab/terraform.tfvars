domain = "lab.bradlab.tech"

trust_net = {
  vsphere_name = "newServers_30"
  name         = "trust"
  cidr         = "192.168.30.0/24"
  gateway      = "192.168.30.1"
  vlan         = "30"
}

dmz_net = {
  vsphere_name = "newDMZ_40"
  name         = "dmz"
  cidr         = "192.168.40.0/24"
  gateway      = "192.168.40.1"
  vlan         = "40"
}

workstations_net = {
  vsphere_name = "newWorkstations_50"
  name         = "workstations"
  cidr         = "192.168.50.0/24"
  gateway      = "192.168.50.1"
  vlan         = "50"
}

mgmt_net = {
  vsphere_name = "mgmt_2"
  name         = "mgmt"
  cidr         = "192.168.2.0/24"
  gateway      = "192.168.2.1"
  vlan         = "2"
}

storage_net = {
  vsphere_name = "storage_20"
  name         = "storage"
  cidr         = "192.168.20.0/24"
  gateway      = "192.168.20.1"
  vlan         = "20"
}

vm_roles = ["guacamole", "rancher", "az_build", "consul", "base", "postgres"]
