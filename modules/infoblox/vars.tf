variable "dmz_cidr" {
  default = ""
}

variable "trust_cidr" {
  default = ""
}

variable "mgmt_cidr" {
  default = ""
}

variable "storage_cidr" {
  default = ""
}

variable "workstations_cidr" {
  default = ""
}
