data "vsphere_datacenter" "dc" {
  name = var.datacenter
}

data "vsphere_resource_pool" "pool" {
  name          = "noncritical"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.vm_template
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.vm_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "vm" {
  count            = var.vm_count
  name             = "${var.vm_name_prefix}${format("%d", count.index + 1)}"
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = var.vm_folder
  num_cpus         = 2
  memory           = 2048
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  scsi_type        = data.vsphere_virtual_machine.template.scsi_type
  resource_pool_id = data.vsphere_resource_pool.pool.id
  annotation       = "provisioned by terraform"

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "disk0"
    size             = data.vsphere_virtual_machine.template.disks[0].size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks[0].eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks[0].thin_provisioned
  }

  disk {
    label       = "disk1"
    size        = "10"
    unit_number = 1
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = "${var.vm_name_prefix}${format("%d", count.index + 1)}"
        domain    = var.domain
      }
      network_interface {}

    }
  }

  tags = ["${var.vm_tag}"]

  provisioner "chef" {
    environment = "lab"

    # run_list        = ["cookbook::base"]
    use_policyfile  = true
    policy_group    = var.chef_group
    policy_name     = "base"
    node_name       = "${var.vm_name_prefix}${format("%d", count.index + 1)}"
    server_url      = "https://chefserv.${var.domain}/organizations/lab"
    recreate_client = true
    user_name       = var.chef_user
    user_key        = file(var.chef_key)
    client_options  = ["chef_license 'accept'"]

    # If you have a self signed cert on your chef server change this to :verify_none
    fetch_chef_certificates = true
    ssl_verify_mode         = ":verify_peer"

    connection {
      host     = self.default_ip_address
      type     = "ssh"
      user     = "root"
      password = var.initial_password
    }
  }
}

