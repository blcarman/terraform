variable "dmz_name" {
  description = "name of dmz portgroup"
  type        = string
}

variable "trust_name" {
  description = "name of trust portgroup"
  type        = string
}

variable "workstations_name" {
  description = "name of workstations portgroup"
  type        = string
}

variable "dmz_vlan" {
  description = "name of dmz vlan"
  type        = string
}

variable "trust_vlan" {
  description = "name of trust vlan"
  type        = string
}

variable "workstations_vlan" {
  description = "name of workstations portgroup"
  type        = string
}

variable "vm_folders" {
  description = "vsphere folders for VMs"
  type        = list(string)
  default     = ["infra", "test", "prod"]
}

variable "vm_roles" {
  description = "available roles for VMs"
  type        = list(string)
}
