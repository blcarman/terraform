variable "datacenter" {
  description = "name of vsphere datacenter"
  type        = string
  default     = "HomeLab"
}

variable "vm_folder" {
  description = "vsphere folder for VMs"
  type        = string
  default     = "test"
}

variable "vm_template" {
  description = "vm template to clone from"
  type        = string
  default     = "el7_packer_ci"
}

variable "vm_count" {
  description = "number of VMs"
  type        = number
  default     = 1
}

variable "vm_datastore" {
  description = "datastore for VMs"
  type        = string
  default     = "warehouseVMs"
}

variable "cidr" {
}

variable "gateway" {
}

variable "network_name" {
}

variable "domain" {
}

variable "chef_user" {
}

variable "chef_key" {
}

variable "chef_group" {
}

variable "initial_password" {}

variable "vm_name_prefix" {}

variable "role" {
  type = string
}
