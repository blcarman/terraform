resource "infoblox_network" "DMZ" {
  network_name = "DMZ"
  cidr         = "${var.dmz_cidr}"
  tenant_id    = "lab"
  reserve_ip   = 20
}

resource "infoblox_network" "trust" {
  network_name = "trust"
  cidr         = "${var.trust_cidr}"
  tenant_id    = "lab"
  reserve_ip   = 50
}

resource "infoblox_network" "mgmt" {
  network_name = "mgmt"
  cidr         = "${var.mgmt_cidr}"
  tenant_id    = "lab"
}

resource "infoblox_network" "storage" {
  network_name = "storage"
  cidr         = "${var.storage_cidr}"
  tenant_id    = "lab"
  reserve_ip   = 10
}

resource "infoblox_network" "workstations" {
  network_name = "workstations"
  cidr         = "${var.workstations_cidr}"
  tenant_id    = "lab"
}
