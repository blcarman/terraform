variable "VC_USERNAME" {
  default = "bradmin"
}

variable "VC_PASSWORD" {
  type    = string
  default = "Pa55word"
}

variable "vsphere_server" {
  default = "vcenter.lab.bradlab.tech"
}

variable "trust_net" {
  type = map(string)
}

variable "dmz_net" {
  type = map(string)
}

variable "workstations_net" {
  type = map(string)
}

variable "mgmt_net" {
  type = map(string)
}

variable "storage_net" {
  type = map(string)
}

variable "domain" {
}

variable "CHEF_USER" {
}

variable "CHEF_KEY" {
}

variable "vm_roles" {}
