/*
leave commented since datacenters can't be imported
resource "vsphere_datacenter" "dc" {
  name = "HomeLab"
}
*/
data "vsphere_datacenter" "dc" {
  name = "HomeLab"
}

# eventually change this from data to resource
data "vsphere_distributed_virtual_switch" "dvs" {
  name          = "DSwitch 2"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# resource "vsphere_distributed_virtual_switch" "dvs1" {}

resource "vsphere_distributed_port_group" "dmz" {
  name                            = var.dmz_name
  distributed_virtual_switch_uuid = data.vsphere_distributed_virtual_switch.dvs.id
  vlan_id                         = var.dmz_vlan
  block_override_allowed          = true
  port_config_reset_at_disconnect = true
}

resource "vsphere_distributed_port_group" "trust" {
  name                            = var.trust_name
  distributed_virtual_switch_uuid = data.vsphere_distributed_virtual_switch.dvs.id
  vlan_id                         = var.trust_vlan
  block_override_allowed          = true
  port_config_reset_at_disconnect = true
}

resource "vsphere_distributed_port_group" "workstations" {
  name                            = var.workstations_name
  distributed_virtual_switch_uuid = data.vsphere_distributed_virtual_switch.dvs.id
  vlan_id                         = var.workstations_vlan
  block_override_allowed          = true
  port_config_reset_at_disconnect = true
}

resource "vsphere_folder" "folder" {
  for_each      = toset(var.vm_folders)
  path          = each.value
  type          = "vm"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_tag_category" "VMs" {
  name = "VMs"
}

resource "vsphere_tag" "tag" {
  for_each    = toset(var.vm_roles)
  name        = each.value
  category_id = data.vsphere_tag_category.VMs.id
}
