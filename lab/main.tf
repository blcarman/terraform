terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "bradlab"

    workspaces {
      name = "lab"
    }
  }
}

provider "vsphere" {
  user                 = var.VC_USERNAME
  password             = var.VC_PASSWORD
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
}

module "vcenter" {
  source            = "../modules/vcenter"
  trust_name        = var.trust_net["vsphere_name"]
  dmz_name          = var.dmz_net["vsphere_name"]
  workstations_name = var.workstations_net["vsphere_name"]
  trust_vlan        = var.trust_net["vlan"]
  dmz_vlan          = var.dmz_net["vlan"]
  workstations_vlan = var.workstations_net["vlan"]
  vm_roles          = var.vm_roles
}

module "guac" {
  source           = "../modules/vm_ansible"
  network_name     = var.trust_net["vsphere_name"]
  cidr             = var.trust_net["cidr"]
  gateway          = var.trust_net["gateway"]
  domain           = "lab.bradlab.tech"
  chef_user        = var.CHEF_USER
  chef_key         = var.CHEF_KEY
  chef_group       = "testgroup"
  initial_password = "Pa55word"
  vm_name_prefix   = "guac"
  role             = "guacamole"
  vm_template      = "el7_packer_ci_261"
}

module "bld" {
  source           = "../modules/vm"
  network_name     = var.trust_net["vsphere_name"]
  cidr             = var.trust_net["cidr"]
  gateway          = var.trust_net["gateway"]
  domain           = "lab.bradlab.tech"
  chef_user        = var.CHEF_USER
  chef_key         = var.CHEF_KEY
  chef_group       = "testgroup"
  initial_password = "Pa55word"
  vm_name_prefix   = "bld"
  role             = "az_build"
  vm_tag           = module.vcenter.vm_tags["az_build"].id
  vm_template      = "el7_packer_ci_261"
}

module "postgres" {
  source           = "../modules/vm_chef"
  network_name     = var.trust_net["vsphere_name"]
  cidr             = var.trust_net["cidr"]
  gateway          = var.trust_net["gateway"]
  domain           = "lab.bradlab.tech"
  chef_user        = var.CHEF_USER
  chef_key         = var.CHEF_KEY
  chef_role        = "postgres"
  initial_password = "Pa55word"
  vm_name_prefix   = "pgsql"
  role             = "postgres"
  vm_tag           = module.vcenter.vm_tags["postgres"].id
  vm_template      = "el8_packer_ci_258"
}

# module "consul" {
#   source           = "../modules/vm"
#   network_name     = var.trust_net["vsphere_name"]
#   cidr             = var.trust_net["cidr"]
#   gateway          = var.trust_net["gateway"]
#   domain           = "lab.bradlab.tech"
#   chef_user        = var.CHEF_USER
#   chef_key         = var.CHEF_KEY
#   chef_group       = "testgroup"
#   initial_password = "Pa55word"
#   vm_name_prefix   = "cons"
#   role             = "consul"
#   vm_tag           = module.vcenter.vm_tags["consul"].id
#   vm_template      = "el8_packer_ci_258"
#   vm_count         = 1
# }